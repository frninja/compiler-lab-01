﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03
{
        public class Lexer
        {
            private int _pos = 0;

            private StringBuilder _sb = new StringBuilder();

            private char _ch;

            public Lexer()
            {
            }

            // Целое со знаком, начинающееся не с цифры 0.
            public void Scan()
            {
                NextCh();
                if (_ch == '+' || _ch == '-')
                {
                    _sb.Append(_ch);
                    NextCh();
                }

                if (char.IsDigit(_ch) && _ch != '0')
                {
                    _sb.Append(_ch);
                    NextCh();
                }
                else
                {
                    Error();
                }

                while (char.IsDigit(_ch))
                {
                    _sb.Append(_ch);
                    NextCh();
                }

                if (_ch != '\r')
                {
                    Error();
                }

                int num = int.Parse(_sb.ToString());

                Console.WriteLine("Integer recognized: {0}", num);
            }

            private void Error()
            {
                Console.WriteLine("^:{0}", _pos);
                Console.WriteLine("Error in character {0}", _ch);
                throw new LexerException();
            }

            private void NextCh()
            {
                _ch = (char)Console.Read();
                ++_pos;
            }

        }
}
