﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task07
{
        public class Lexer
        {
            private int _pos = 0;

            private char _ch;

            private StringBuilder _sb = new StringBuilder();

            public Lexer()
            {
            }

            // Вещественное с десятичной точкой 123.45678.
            public void Scan()
            {

                NextCh();

                if (char.IsDigit(_ch))
                {
                    _sb.Append(_ch);
                    NextCh();
                }
                else
                {
                    Error();
                }

                while (char.IsDigit(_ch))
                {
                    _sb.Append(_ch);
                    NextCh();
                }

                if (_ch == '.')
                {
                    _sb.Append(_ch);
                    NextCh();
                }
                else
                {
                    Error();
                }

                if (char.IsDigit(_ch))
                {
                    while (char.IsDigit(_ch))
                    {
                        _sb.Append(_ch);
                        NextCh();
                    }
                }
                else
                {
                    Error();
                }

                if (_ch != '\r')
                {
                    Error();
                }


                Console.Write("String: {0}", _sb.ToString());
            }

            private void Error()
            {
                Console.WriteLine("^:{0}", _pos);
                Console.WriteLine("Error in character {0}", _ch);
                throw new LexerException();
            }

            private void NextCh()
            {
                _ch = (char)Console.Read();
                ++_pos;
            }

        }
}
