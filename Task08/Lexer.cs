﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task08
{
        public class Lexer
        {
            private int _pos = 0;

            private char _ch;

            private StringBuilder _sb = new StringBuilder();

            public Lexer()
            {
            }

            // Лексема вида 'строка', внутри апострофов отсутствует символ '
            public void Scan()
            {

                NextCh();

                if (_ch == '\'')
                {
                    _sb.Append(_ch);
                    NextCh();
                }
                else
                {
                    Error();
                }

                while (_ch != '\'')
                {
                    _sb.Append(_ch);
                    NextCh();
                }

                if (_ch == '\'')
                {
                    _sb.Append(_ch);
                    NextCh();
                }
                else
                {
                    Error();
                }

                if (_ch != '\r')
                {
                    Error();
                }


                Console.Write("String: {0}", _sb.ToString());
            }

            private void Error()
            {
                Console.WriteLine("^:{0}", _pos);
                Console.WriteLine("Error in character {0}", _ch);
                throw new LexerException();
            }

            private void NextCh()
            {
                _ch = (char)Console.Read();
                ++_pos;
            }

        }
}
