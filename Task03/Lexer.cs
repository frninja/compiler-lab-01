﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03
{
        public class Lexer
        {
            private int _pos = 0;

            private char _ch;

            private StringBuilder _sb = new StringBuilder();

            public Lexer()
            {
            }

            // Чередующиеся буквы и цифры, начинающиеся с буквы.
            public void Scan()
            {

                NextCh();

                if (char.IsLetter(_ch))
                {
                    _sb.Append(_ch);
                    NextCh();
                }
                else
                {
                    Error();
                }

                if (char.IsDigit(_ch))
                {
                    _sb.Append(_ch);
                    NextCh();
                }
                else
                {
                    Error();
                }

                while (true)
                {
                    if (char.IsLetter(_ch))
                    {
                        _sb.Append(_ch);
                        NextCh();
                    }
                    else
                    {
                        break;
                    }
                    if (char.IsDigit(_ch))
                    {
                        _sb.Append(_ch);
                        NextCh();
                    }
                    else
                    {
                        break;
                    }
                }
                
                if (_ch != '\r')
                {
                    Error();
                }

            }

            private void Error()
            {
                Console.WriteLine("^:{0}", _pos);
                Console.WriteLine("Error in character {0}", _ch);
                throw new LexerException();
            }

            private void NextCh()
            {
                _ch = (char)Console.Read();
                ++_pos;
            }

        }
}
