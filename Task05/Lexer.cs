﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task05
{
        public class Lexer
        {
            private int _pos = 0;

            private char _ch;

            private List<char> _digits = new List<char>();

            public Lexer()
            {
            }

            // Список цифр, разделенных одним или несколькими пробелами. В качестве семантического действия должно быть накопление списка цифр в списке и вывод этого списка в конце программы.
            public void Scan()
            {

                NextCh();

                if (char.IsDigit(_ch))
                {
                    _digits.Add(_ch);
                    NextCh();
                }
                else if (_ch != '\r')
                {
                    Error();
                }

                while (true)
                {
                    if (_ch == ' ')
                    {
                        NextCh();
                    }
                    else
                    {
                        break;
                    }

                    while (_ch == ' ')
                    {
                        NextCh();
                    }

                    if (char.IsDigit(_ch))
                    {
                        _digits.Add(_ch);
                        NextCh();
                    }
                    else
                    {
                        Error();
                    }
                }
                
                if (_ch != '\r')
                {
                    Error();
                }


                if (_ch == ' ')
                {
                    while (true)
                    {
                        if (_ch == ',' || _ch == ';')
                        {
                            NextCh();
                        }
                        else
                        {
                            break;
                        }
                        if (char.IsLetter(_ch))
                        {
                            _digits.Add(_ch);
                            NextCh();
                        }
                        else
                        {
                            Error();
                        }
                    }
                }
                
                if (_ch != '\r')
                {
                    Error();
                }

                Console.Write("Chars: ");
                foreach (var c in _digits)
                {
                    Console.Write("{0} ", c);
                }
            }

            private void Error()
            {
                Console.WriteLine("^:{0}", _pos);
                Console.WriteLine("Error in character {0}", _ch);
                throw new LexerException();
            }

            private void NextCh()
            {
                _ch = (char)Console.Read();
                ++_pos;
            }

        }
}
