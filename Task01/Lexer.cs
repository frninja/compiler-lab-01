﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02
{
        public class Lexer
        {
            private int _pos = 0;

            private StringBuilder _sb = new StringBuilder();

            private char _ch;

            public Lexer()
            {
            }

            // Идентификатор.
            public void Scan()
            {
                NextCh();

                if (char.IsLetter(_ch))
                {
                    _sb.Append(_ch);
                    NextCh();
                }
                else
                {
                    Error();
                }

                while (char.IsLetter(_ch) | char.IsDigit(_ch))
                {
                    _sb.Append(_ch);
                    NextCh();
                }

                if (_ch != '\r')
                {
                    Error();
                }

                Console.WriteLine("Id recognized: {0}", _sb.ToString());
            }

            private void Error()
            {
                Console.WriteLine("^:{0}", _pos);
                Console.WriteLine("Error in character {0}", _ch);
                throw new LexerException();
            }

            private void NextCh()
            {
                _ch = (char)Console.Read();
                ++_pos;
            }

        }
}
