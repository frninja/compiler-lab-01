﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task06
{
        public class Lexer
        {
            private int _pos = 0;

            private char _ch;

            private StringBuilder _sb = new StringBuilder();

            public Lexer()
            {
            }

            // Лексема вида aa12c23dd1, в которой чередуются группы букв и цифр, в каждой группе не более 2 элементов. В качестве семантического действия необходимо накопить данную лексему в виде строки.
            public void Scan()
            {

                NextCh();

                if (char.IsLetter(_ch))
                {
                    _sb.Append(_ch);
                    NextCh();
                }
                else
                {
                    Error();
                }

                if (char.IsLetter(_ch))
                {
                    _sb.Append(_ch);
                    NextCh();
                }
                
                if (char.IsDigit(_ch))
                {
                    _sb.Append(_ch);
                    NextCh();
                }
                else
                {
                    Error();
                }

                if (char.IsDigit(_ch))
                {
                    _sb.Append(_ch);
                    NextCh();
                }

                while (true)
                {
                    if (char.IsLetter(_ch))
                    {
                        _sb.Append(_ch);
                        NextCh();
                    }
                    else
                    {
                        break;
                    }

                    if (char.IsLetter(_ch))
                    {
                        _sb.Append(_ch);
                        NextCh();
                    }

                    if (char.IsDigit(_ch))
                    {
                        _sb.Append(_ch);
                        NextCh();
                    }
                    else
                    {
                        break;
                    }

                    if (char.IsDigit(_ch))
                    {
                        _sb.Append(_ch);
                        NextCh();
                    }
                }
                
                if (_ch != '\r')
                {
                    Error();
                }


                Console.Write("String: {0}", _sb.ToString());
            }

            private void Error()
            {
                Console.WriteLine("^:{0}", _pos);
                Console.WriteLine("Error in character {0}", _ch);
                throw new LexerException();
            }

            private void NextCh()
            {
                _ch = (char)Console.Read();
                ++_pos;
            }

        }
}
