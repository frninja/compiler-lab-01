﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task09
{
    class Program
    {
        static void Main(string[] args)
        {
            Lexer lexer = new Lexer();
            try
            {
                lexer.Scan();
            }
            catch (LexerException e)
            {
                Console.WriteLine(e);
            }
            Console.ReadKey();
        }
    }
}
