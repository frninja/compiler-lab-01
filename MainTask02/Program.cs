﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01
{
    class Program
    {
        static void Main(string[] args)
        {
            //string text = Console.ReadLine();
            Lexer lexer = new Lexer();
            try
            {
                lexer.Scan();
            }
            catch (LexerException e)
            {
                Console.WriteLine(e);
            }
            Console.ReadKey();
        }
    }
}
